﻿// -----------------------------------------------------------------------------
// <copyright file="UnitTest.cs" company="genuine">
//      Copyright (c) .NET Jig Team. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------------
namespace Jig.VisualStudio.Extensions.RazorFileGenerator.UnitTestProject
{
    using System;
    using System.IO;
    using System.Reflection;

    using Jig.VisualStudio.Extensions.RazorFileGenerator;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The unit test.
    /// </summary>
    [TestClass]
    public partial class UnitTest
    {
        /// <summary>
        /// The test view page 01.
        /// </summary>
        [TestMethod]
        public void TestViewPage01()
        {
            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                string[] resources = assembly.GetManifestResourceNames();
                foreach (var resource in resources)
                {
                    var stream = assembly.GetManifestResourceStream(resource);
                    var reader = new StreamReader(stream);
                    var cshtml = reader.ReadToEnd();
                    var razorTemplateCompiler = new RazorTemplateCompiler();
                    var source = razorTemplateCompiler.Compile(cshtml, resource);
                    var baseDir = AppDomain.CurrentDomain.BaseDirectory + "\\";
                    File.WriteAllText(baseDir + razorTemplateCompiler.CompilerProcessorArgs.ClassName + ".cs", source);
                }
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }
    }
}
