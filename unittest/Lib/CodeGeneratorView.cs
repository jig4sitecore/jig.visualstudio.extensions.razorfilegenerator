﻿namespace Jig.Web.CodeGeneration.Views
{
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The Jig razor base.
    /// </summary>
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public partial class CodeGeneratorView
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        /// <summary>
        /// Renders the view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected virtual void RenderView(HtmlTextWriter output)
        {
            var code = new XElement("code", string.Empty);
            code.Add(new XAttribute("class", "error"));
            code.Add(
                new XAttribute(
                    "data-error-message",
                    "Override " + this.GetType().FullName + ".RenderView() method"));

            output.WriteLine(code);
        }
    }
}
