﻿namespace Jig.Web.CodeGeneration.Views
{
    /// <summary>
    /// The Jig razor base.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public partial class CodeGeneratorView<TModel> : CodeGeneratorView where TModel : class
    {
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public TModel Model { get; set; }
    }
}
