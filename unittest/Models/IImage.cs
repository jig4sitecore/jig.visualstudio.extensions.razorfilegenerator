﻿namespace MvcApplication
{
    /// <summary>
    /// The Image interface.
    /// </summary>
    public interface IImage
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the alt.
        /// </summary>
        string Alt { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Gets or sets the src.
        /// </summary>
        string Src { get; set; }

        #endregion
    }
}