﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.UnitTestProject
{
    /// <summary>
    /// The address model.
    /// </summary>
    public class AddressModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the address line 1.
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the address line 2.
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zi code.
        /// </summary>
        public string ZiCode { get; set; }

        #endregion
    }
}