﻿namespace MvcApplication
{
    /// <summary>
    /// The SideVideoGroupItem interface.
    /// </summary>
    public interface ISideVideoGroupItem
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the side video group item blurb.
        /// </summary>
        string SideVideoGroupItemBlurb { get; set; }

        /// <summary>
        /// Gets or sets the side video group item image.
        /// </summary>
        IImage SideVideoGroupItemImage { get; set; }

        /// <summary>
        /// Gets or sets the side video group item title.
        /// </summary>
        string SideVideoGroupItemTitle { get; set; }

        /// <summary>
        /// Gets or sets the side video group item youtube.
        /// </summary>
        string SideVideoGroupItemYoutube { get; set; }

        #endregion
    }
}