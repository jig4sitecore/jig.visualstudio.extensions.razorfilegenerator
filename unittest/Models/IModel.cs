﻿namespace MvcApplication
{
    using System.Collections.Generic;

    /// <summary>
    /// The SideVideoGroup interface.
    /// </summary>
    public interface ISideVideoGroup
    {
        #region Public Properties

        /// <summary>
        /// Gets the side video group list.
        /// </summary>
        ICollection<ISideVideoGroupItem> SideVideoGroupList { get; }

        /// <summary>
        /// Gets or sets the side video group title.
        /// </summary>
        string SideVideoGroupTitle { get; set; }

        #endregion
    }
}