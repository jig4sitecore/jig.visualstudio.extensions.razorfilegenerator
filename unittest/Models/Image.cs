﻿namespace MvcApplication
{
    /// <summary>
    /// The image.
    /// </summary>
    public class Image : IImage
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the alt.
        /// </summary>
        public string Alt { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the src.
        /// </summary>
        public string Src { get; set; }

        #endregion
    }
}
