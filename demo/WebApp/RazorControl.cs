﻿namespace Minion.Web.UI
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.WebPages;
    using System.Web.WebPages.Instrumentation;

    /// <summary>
    /// The razor control.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    /// <typeparam name="TViewBag">The type of the view bag.</typeparam>
    [ControlBuilder(typeof(LiteralControlBuilder)), ParseChildren(false), PersistChildren(false)]
    public abstract partial class RazorControl<TModel, TViewBag> : Control
        where TModel : class, new()
        where TViewBag : class, new()
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        public TModel Model { get; set; }

        /// <summary>
        /// Gets or sets the view bag.
        /// </summary>
        public TViewBag ViewBag { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The render control.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            var type = this.GetType();
            writer.WriteLine("<!-- Begin:" + type.FullName + "-->");

            try
            {
                if (this.Model != null)
                {
                    /* Do proper exception handling, write to inner writer  */
                    using (var innerWriter = new XhtmlTextWriter(new StringWriter(new StringBuilder(512))))
                    {
                        this.RenderView(innerWriter);
                        writer.WriteLine(innerWriter.InnerWriter.ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                /* Do logging and disable caching */
                Trace.TraceError(exception.ToString());
                writer.WriteLine("<!-- " + exception.Message + "-->");
            }

            writer.WriteLine("<!-- End:" + type.Name + "-->");
        }

        /// <summary>
        /// Renders the control view.
        /// </summary>
        /// <param name="writer">The writer.</param>
        protected abstract void RenderView(HtmlTextWriter writer);

        #endregion

        #region Methods

        /// <summary>
        /// Razor 2.0
        /// Writes attribute in situations like &lt;img src="@Model"&gt;.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="suffix">The suffix.</param>
        /// <param name="values">The values.</param>
        protected void WriteRazorAttribute(
            HtmlTextWriter writer,
            string attribute,
            PositionTagged<string> prefix,
            PositionTagged<string> suffix,
            params AttributeValue[] values)
        {
            if (writer != null)
            {
                writer.Write(prefix.Value);

                if (values != null)
                {
                    foreach (var attributeValue in values)
                    {
                        writer.Write(attributeValue.Prefix.Value);

                        var value = attributeValue.Value.Value;
                        if (value != null)
                        {
                            writer.Write(value);
                        }
                    }
                }

                writer.Write(suffix.Value);
            }
        }

        #endregion
    }
}