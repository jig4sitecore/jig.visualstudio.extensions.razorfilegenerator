﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div>
        <ul>
            <li>Install the 'Minion.Razor.Generator.vsix' package, which registers a special view engine</li>
            <li>Go to an Razor view's property and set the Custom tool to MinionRazorGenerator</li>
            <li>You'll see a generated .cs file under the .cshtml file, which will be used at runtime instead of the .cshtml file</li>
            <li>Add file ~/views/Web.config to the any root folder to enable Razor IntelliSense for tools like Resharper or VS</li>
        </ul>
        <h2>Minion.Razor.Generator <span style="color: red">Required</span> directives</h2>
        <pre style="color: #008000">            @* ClassNamespace = {NameSpace} *@
           @* ClassName = {ClassName} *@
        </pre>
        <h2>Minion.Razor.Generator <span style="color: red">Optional</span> directives</h2>
        <pre style="color: #008000">            @* ClassMethod = {ClassMethodName} *@
           @* IsClassMethodOverride = {Is Class Method Override} *@
        </pre>
        <p>
            Default <span style="color: green">ClassMethod</span> is RenderView
        </p>
        <p>
            Default <span style="color: green">IsClassMethodOverride</span> is true
        </p>
        <p>
            Example: protected <span style="color: green"> override</span> void <span style="color: green">RenderView</span> (HtmlTextWriter writer)
        </p>
    </div>
</body>
</html>
