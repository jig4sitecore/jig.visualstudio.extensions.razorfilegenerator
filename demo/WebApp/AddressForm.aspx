﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div>
        <minion:AddressView runat="server" />
    </div>
    <div>
        <minion:AddressDeviceAdaptiveRenderingView ContextDevice="Browser" runat="server" />
        <minion:AddressDeviceAdaptiveRenderingView ContextDevice="Mobile" runat="server" />
        <minion:AddressDeviceAdaptiveRenderingView ContextDevice="Tablet" runat="server" />
    </div>
</body>
</html>
