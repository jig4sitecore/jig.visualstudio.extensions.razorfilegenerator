﻿namespace WebApplication.Views
{
    using System;
    using System.Web.UI;

    using Minion.Web.UI;

    using WebApplication.Models;

    /// <summary>
    ///     The address view.
    /// </summary>
    public partial class AddressDeviceAdaptiveRenderingView : RazorControl<AddressModel, dynamic>
    {
        #region Enums

        /// <summary>
        /// The device.
        /// </summary>
        public enum Device
        {
            /// <summary>
            /// The browser.
            /// </summary>
            Browser,

            /// <summary>
            /// The mobile.
            /// </summary>
            Mobile,

            /// <summary>
            /// The tablet.
            /// </summary>
            Tablet
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the context device.
        /// </summary>
        public Device ContextDevice { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Model = new AddressModel
                             {
                                 AddressLine1 = "500 Harrison Ave",
                                 AddressLine2 = "5th floor",
                                 City = "Boston",
                                 State = "MA",
                                 ZiCode = "02187"
                             };
        }

        /// <summary>
        /// The render view.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        protected override void RenderView(HtmlTextWriter writer)
        {
            switch (this.ContextDevice)
            {
                case Device.Mobile:
                    this.RenderMobileView(writer);
                    break;
                case Device.Tablet:
                    this.RenderTabletView(writer);
                    break;
                default:
                    this.RenderDesktopView(writer);
                    break;
            }
        }

        #endregion
    }
}