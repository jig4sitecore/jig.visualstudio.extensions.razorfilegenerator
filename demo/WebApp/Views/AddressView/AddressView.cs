﻿namespace WebApplication.Views
{
    using System;
    using System.Diagnostics;
    using System.Web.UI;

    using Minion.Web.UI;

    using WebApplication.Models;

    /// <summary>
    /// The address view.
    /// </summary>
    public partial class AddressView : RazorControl<AddressModel, dynamic>
    {
        #region Methods

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">The e.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Model = new AddressModel
                             {
                                 AddressLine1 = "500 Harrison Ave",
                                 AddressLine2 = "5th floor",
                                 City = "Boston",
                                 State = "MA",
                                 ZiCode = "02187"
                             };
        }

        #endregion
    }
}