﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;

    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Designer.Interfaces;
    using Microsoft.VisualStudio.OLE.Interop;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;

    using IServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;

    /// <summary>
    /// The diamond generator.
    /// </summary>
    /// <remarks>The GUID of Generator must match source.extension.vsixmanifest 
    /// <![CDATA[<Identifier Id="20160125-AE13-4714-B3D2-14D654FC3FF3">]]>
    /// </remarks>
    [ComVisible(true)]
    [Guid(JigRazorGenerator.GuidFileGeneratorString)]
    [ProvideObject(typeof(JigRazorGenerator))]
    [CodeGeneratorRegistration(typeof(JigRazorGenerator), "JigRazorGenerator", VsContextGuidVcsProject, GeneratesDesignTimeSource = true)]
    public class JigRazorGenerator : IVsSingleFileGenerator, IObjectWithSite
    {
        /// <summary>
        /// The vs context unique identifier VCS project
        /// </summary>
        public const string VsContextGuidVcsProject = "{FAE04EC1-301F-11D3-BF4B-00C04F79EFBC}";

        /// <summary>
        /// The guid diamond file generator string.
        /// </summary>
        public const string GuidFileGeneratorString = "20160125-AE13-4714-B3D2-14D654FC3FF3";

        /// <summary>
        /// The guid diamond file generator.
        /// </summary>
        public static readonly Guid GuidFileGenerator = new Guid(GuidFileGeneratorString);

        /// <summary>
        /// Gets or sets the code DOM provider.
        /// </summary>
        /// <value>
        /// The code DOM provider.
        /// </value>
        private CodeDomProvider CodeDomProvider { get; set; }

        /// <summary>
        /// Gets or sets the service provider.
        /// </summary>
        /// <value>
        /// The service provider.
        /// </value>
        private ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        /// <value>
        /// The site.
        /// </value>
        private object Site { get; set; }

        /// <summary>
        /// Gets the code provider.
        /// </summary>
        private CodeDomProvider CodeProvider
        {
            get
            {
                if (this.CodeDomProvider == null)
                {
                    var provider = (IVSMDCodeDomProvider)this.SiteServiceProvider.GetService(typeof(IVSMDCodeDomProvider).GUID);
                    if (provider != null)
                    {
                        this.CodeDomProvider = (CodeDomProvider)provider.CodeDomProvider;
                    }
                }

                return this.CodeDomProvider;
            }
        }

        /// <summary>
        /// Gets the site service provider.
        /// </summary>
        private ServiceProvider SiteServiceProvider
        {
            get
            {
                if (this.ServiceProvider == null)
                {
                    var oleServiceProvider = this.Site as IServiceProvider;
                    this.ServiceProvider = new ServiceProvider(oleServiceProvider);
                }

                return this.ServiceProvider;
            }
        }

        /// <summary>
        /// Generated file default extension.
        /// </summary>
        /// <param name="defaultExtension">The default extension.</param>
        /// <returns>
        /// The VSConstants.
        /// </returns>
        public int DefaultExtension(out string defaultExtension)
        {
            defaultExtension = ".Generated." + this.CodeProvider.FileExtension;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// The generate.
        /// </summary>
        /// <param name="inputFilePath">The input file path.</param>
        /// <param name="inputFileContents">The input file contents.</param>
        /// <param name="defaultNamespace">The default namespace.</param>
        /// <param name="outputFileContents">The output file contents.</param>
        /// <param name="output">The output.</param>
        /// <param name="generateProgress">The generate progress.</param>
        /// <returns>The <see cref="int" />.</returns>
        public int Generate(string inputFilePath, string inputFileContents, string defaultNamespace, IntPtr[] outputFileContents, out uint output, IVsGeneratorProgress generateProgress)
        {
            try
            {
                if (inputFileContents == null)
                {
                    var ex = new ArgumentException("inputFileContents");
                    Trace.TraceError(ex.Message);
                    throw ex;
                }

                using (var razorTemplateCompiler = new RazorTemplateCompiler())
                {
                    /* This name should never be used except unit testing dynamic/on fly generation & compile times  */
                    string suggestedClassNameByFileName = "Generated" + Guid.NewGuid().ToString("N");
                    if (!string.IsNullOrWhiteSpace(inputFilePath))
                    {
                        try
                        {
                            /* This is code added more like convenience for simple classNames. 
                             * Its makes @ ClassMethod ={Name} @ optional  */
                            var filename = Path.GetFileNameWithoutExtension(inputFilePath);
                            var className = new StringBuilder(filename);
                            className.Replace(".", string.Empty);
                            className.Replace("-", string.Empty);
                            className.Replace("_", string.Empty);

                            suggestedClassNameByFileName = className.ToString();
                        }
                        catch (Exception exception)
                        {
                            Trace.TraceError(exception.Message);
                        }
                    }

                    var source = razorTemplateCompiler.Compile(inputFileContents, suggestedClassNameByFileName);
                    byte[] bytes = Encoding.UTF8.GetBytes(source ?? string.Empty);

                    outputFileContents[0] = Marshal.AllocCoTaskMem(bytes.Length);
                    Marshal.Copy(bytes, 0, outputFileContents[0], bytes.Length);
                    output = (uint)bytes.Length;
                }

                return VSConstants.S_OK;
            }
            catch (Exception exception)
            {
                Trace.TraceError(exception.ToString());
                if (generateProgress != null)
                {
                    generateProgress.GeneratorError(0, 0, exception.Message, 0, 0);
                }

                outputFileContents[0] = IntPtr.Zero;
                output = 0;
            }

            return VSConstants.E_FAIL;
        }

        /// <summary>
        /// Gets the site.
        /// </summary>
        /// <param name="registrationId">The registration unique identifier.</param>
        /// <param name="site">The site.</param>
        public void GetSite(ref Guid registrationId, out IntPtr site)
        {
            if (this.Site == null)
            {
                Marshal.ThrowExceptionForHR(VSConstants.E_NOINTERFACE);
            }

            // Query for the interface using the site object initially passed to the generator
            IntPtr punk = Marshal.GetIUnknownForObject(this.Site);
            int hr = Marshal.QueryInterface(punk, ref registrationId, out site);
            Marshal.Release(punk);
            ErrorHandler.ThrowOnFailure(hr);
        }

        /// <summary>
        /// The set site.
        /// </summary>
        /// <param name="site">The site.</param>
        public void SetSite(object site)
        {
            this.Site = site;

            this.CodeDomProvider = null;
            this.ServiceProvider = null;
        }
    }
}
