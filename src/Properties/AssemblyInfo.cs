﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("A6D52FEE-03BA-4E53-B28F-1E8614A322C4")]
[assembly: AssemblyProduct("JigRazorGenerator")]
[assembly: AssemblyDescription("JigRazorGenerator")]
[assembly: AssemblyCompany(".NET Jig Web Development Team")]

[assembly: AssemblyCopyright("Copyright © .NET Jig Team")]
[assembly: AssemblyTrademark(".NET Jig")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyFileVersion("2.0.0")]
[assembly: AssemblyVersion("2.0.0")]