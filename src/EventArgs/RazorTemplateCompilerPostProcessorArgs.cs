﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Class RazorTemplateCompilerPostprocessorArgs.
    /// </summary>
    public sealed class RazorTemplateCompilerPostProcessorArgs : RazorTemplateCompilerArgs, 
                                                          IRazorTemplateCompilerPostProcessorArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorTemplateCompilerPostProcessorArgs"/> class.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <param name="fileOutput">The cs file output.</param>
        public RazorTemplateCompilerPostProcessorArgs(IRazorTemplateCompilerArgs args, string fileOutput)
            : base(args)
        {
            Contract.Assert(!string.IsNullOrWhiteSpace(fileOutput), "Cs File output is required!");
            this.CsFileOutput = fileOutput;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the cs file output.
        /// </summary>
        public string CsFileOutput { get; set; }

        #endregion
    }
}