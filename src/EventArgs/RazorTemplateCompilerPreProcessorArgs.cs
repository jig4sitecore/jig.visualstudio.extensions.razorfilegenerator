﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    using System;

    /// <summary>
    /// Class RazorTemplateCompilerPreProcessorArgs.
    /// </summary>
    public sealed class RazorTemplateCompilerPreProcessorArgs : RazorTemplateCompilerArgs, IRazorTemplateCompilerPreProcessorArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RazorTemplateCompilerPreProcessorArgs"/> class.
        /// </summary>
        /// <param name="originalCshtml">The original CSHTML.</param>
        public RazorTemplateCompilerPreProcessorArgs(string originalCshtml)
        {
            if (string.IsNullOrWhiteSpace(originalCshtml))
            {
                throw new ArgumentNullException("originalCshtml", "The original cshtml cannot be empty!");
            }

            this.OriginalCshtml = originalCshtml;

            /* This will be modified by pipeline */
            this.Cshtml = originalCshtml;
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the Razor CsHtml file input.
        /// </summary>
        /// <value>The CsHtml file input.</value>
        public string Cshtml { get; set; }

        #endregion
    }
}