﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    using System;

    /// <summary>
    /// Class RazorTemplateCompilerArgs.
    /// </summary>
    public abstract class RazorTemplateCompilerArgs : System.EventArgs, IRazorTemplateCompilerArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorTemplateCompilerArgs"/> class.
        /// </summary>
        /// <param name="args">The arguments.</param>
        protected RazorTemplateCompilerArgs(IRazorTemplateCompilerArgs args)
            : this()
        {
            if (args == null)
            {
                throw new ArgumentNullException("args", "RazorTemplateCompilerPreProcessorArgs is null!");
            }

            this.ClassMethodName = args.ClassMethodName;
            this.ClassNamespace = args.ClassNamespace;
            this.ClassName = args.ClassName;
            this.IsNotClassMethodOverride = args.IsNotClassMethodOverride;
            this.RemoveCSharpCodeEmptyLines = args.RemoveCSharpCodeEmptyLines;
            this.RemoveEmptyLinesFromRazorFileInput = args.RemoveEmptyLinesFromRazorFileInput;
            this.OriginalCshtml = args.OriginalCshtml;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorTemplateCompilerArgs"/> class.
        /// </summary>
        protected RazorTemplateCompilerArgs()
        {
            this.RemoveCSharpCodeEmptyLines = true;
            this.RemoveEmptyLinesFromRazorFileInput = true;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the class method name.
        /// </summary>
        public string ClassMethodName { get; set; }

        /// <summary>
        /// Gets or sets the class name.
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Gets or sets the class namespace.
        /// </summary>
        public string ClassNamespace { get; set; }

        /// <summary>
        /// Gets or sets the is not class method override.
        /// </summary>
        public bool? IsNotClassMethodOverride { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [remove code empty lines].
        /// </summary>
        /// <value><c>true</c> if [remove code empty lines]; otherwise, <c>false</c>.</value>
        /// <remarks>This VS extension is used for Web and Code/Template generation. This is code generation directive.</remarks>
        public bool RemoveCSharpCodeEmptyLines { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether remove empty lines.
        /// </summary>
        public bool RemoveEmptyLinesFromRazorFileInput { get; set; }

        /// <summary>
        /// Gets or sets the original CSHTML.
        /// </summary>
        /// <value>The original CSHTML.</value>
        public string OriginalCshtml { get; protected set; }

        #endregion
    }
}