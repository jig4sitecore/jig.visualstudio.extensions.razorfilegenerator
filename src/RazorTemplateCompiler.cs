﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web.Razor;
    using System.Web.Razor.Generator;

    using Jig.VisualStudio.Extensions.RazorFileGenerator.PostProcessors;
    using Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors;

    using Microsoft.CSharp;

    /// <summary>
    ///     The template compiler.
    /// </summary>
    public partial class RazorTemplateCompiler : IDisposable
    {
        #region Static Fields

        /// <summary>
        ///     The code provider.
        /// </summary>
        private readonly CodeDomProvider codeProvider = new CSharpCodeProvider();

        /// <summary>
        ///     The host.
        /// </summary>
        private readonly RazorEngineHost razorEngineHost = new RazorEngineHost(new CSharpRazorCodeLanguage());

        /// <summary>
        ///     The engine
        /// </summary>
        private readonly RazorTemplateEngine razorTemplateEngine;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorTemplateCompiler"/> class.
        /// </summary>
        public RazorTemplateCompiler()
        {
            foreach (string defaultNamespace in Properties.Settings.Default.RazorDefaultNamespaces)
            {
                if (!string.IsNullOrWhiteSpace(defaultNamespace))
                {
                    this.razorEngineHost.NamespaceImports.Add(defaultNamespace);
                }
            }

            /* This VS extension is used for Web Control and Code/Template generation as well. 
             * Some of these directives are Code Generation/ Templating specific. */
            this.CompilerPreProcessors = new List<IRazorTemplateCompilerPreProcessor>
                                             {
                                                 new ResolveClassNamespaceDirectivePreProcessor(),
                                                 new ResolveClassNameDirectivePreProcessor(),
                                                 new ResolveClassMethodNameDirectivePreProcessor(),
                                                 new ResolveIsOverrideMethodDirectivePreProcessor(),

                                                 /* Two directives used in  Code Generation/Templating  */
                                                 new ResolveRemoveInputEmptyLinesDirectivePreProcessor(),
                                                 new ResolveRemoveOutputEmptyLinesDirectivePreProcessor(),

                                                 /* Cs Html file cleanup before passing to Razor Template Engine. Its makes more readable/pretty code */
                                                 new PreGenerationCodeCleanupPreProcessor()
                                             };

            this.CompilerPostProcessors = new List<IRazorTemplateCompilerPostProcessor>
                                              {
                                                  new CodeFormattingAndCleanUpPostProcessors(),
                                                  new RemoveEmptyLinesProcessors()
                                              };

            this.razorEngineHost.GeneratedClassContext = new GeneratedClassContext(executeMethodName: "RenderContent", writeMethodName: "writer.Write", writeLiteralMethodName: "writer.Write");
            this.razorTemplateEngine = new RazorTemplateEngine(this.razorEngineHost);
        }

        #endregion

        /// <summary>
        /// Gets the compiler PreProcessors.
        /// </summary>
        /// <value>The compiler PreProcessors.</value>
        public IList<IRazorTemplateCompilerPreProcessor> CompilerPreProcessors { get; private set; }

        /// <summary>
        /// Gets the compiler post processors.
        /// </summary>
        /// <value>The compiler post processors.</value>
        public IList<IRazorTemplateCompilerPostProcessor> CompilerPostProcessors { get; private set; }

        /// <summary>
        /// Gets the compiler processor arguments.
        /// </summary>
        /// <value>The compiler processor arguments.</value>
        /// <remarks>Used for Unit test only</remarks>
        public IRazorTemplateCompilerArgs CompilerProcessorArgs { get; private set; }

        #region Public Methods and Operators

        /// <summary>
        /// Returns template created from specified source.
        /// </summary>
        /// <param name="cshtml">The source.</param>
        /// <param name="suggestedClassNameByFileName">Name of the suggested class name by file.</param>
        /// <returns>The dynamic ITemplate.</returns>
        /// <exception cref="System.ArgumentException">TemplateManager source can't be null or empty string.;source</exception>
        public string Compile(string cshtml, string suggestedClassNameByFileName)
        {
            if (string.IsNullOrEmpty(cshtml))
            {
                throw new ArgumentException("TemplateManager source can't be null or empty string.", "cshtml");
            }

            var preProcessorArgs = new RazorTemplateCompilerPreProcessorArgs(cshtml)
                           {
                               ClassName = suggestedClassNameByFileName
                           };

            /* Required by Unit Test only */
            this.CompilerProcessorArgs = preProcessorArgs;

            foreach (var processor in this.CompilerPreProcessors)
            {
                try
                {
                    processor.Execute(preProcessorArgs);
                }
                catch (Exception exception)
                {
                    Trace.TraceError(exception.Message);
                    throw;
                }
            }

            string fileOutput = this.Compile(preProcessorArgs);

            var postProcessorArgs = new RazorTemplateCompilerPostProcessorArgs(preProcessorArgs, fileOutput);
            foreach (var processor in this.CompilerPostProcessors)
            {
                try
                {
                    processor.Execute(postProcessorArgs);
                }
                catch (Exception exception)
                {
                    Trace.TraceError(exception.Message);
                    throw;
                }
            }

            return postProcessorArgs.CsFileOutput;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.codeProvider != null)
            {
                this.codeProvider.Dispose();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The compile.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>The generated C# code</returns>
        private string Compile(IRazorTemplateCompilerPreProcessorArgs args)
        {
            GeneratorResults results;
            using (var textReader = new StringReader(args.Cshtml))
            {
                results = this.razorTemplateEngine.GenerateCode(textReader, args.ClassName, args.ClassNamespace, null);
            }

            var builder = new StringBuilder(1024);

            using (var writer = new StringWriter(builder, CultureInfo.InvariantCulture))
            {
                this.codeProvider.GenerateCodeFromCompileUnit(
                    results.GeneratedCode, 
                    writer,
                    new CodeGeneratorOptions
                        {
                            BlankLinesBetweenMembers = false
                        });

                return builder.ToString();
            }
        }

        #endregion
    }
}