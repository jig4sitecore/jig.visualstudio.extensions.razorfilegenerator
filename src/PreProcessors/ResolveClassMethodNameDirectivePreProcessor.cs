﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// The resolve class method name PreProcessor.
    /// </summary>
    internal sealed class ResolveClassMethodNameDirectivePreProcessor : IRazorTemplateCompilerPreProcessor
    {
        #region Fields

        /// <summary>
        ///     The class method regex
        /// </summary>
        private readonly Regex classMethodRegex =
            new Regex(
                @"(?:@\x2A \s{1,})(?<Setting>(?<Key>ClassMethod)(?:\s{1,})=(?:\s{1,})(?<Value>(\w|\.|_)+))(?:\s{1,})(?:\x2A@)\s*\n?\r?", 
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Execute(IRazorTemplateCompilerPreProcessorArgs args)
        {
            var match = this.classMethodRegex.Match(args.OriginalCshtml);
            if (match.Success)
            {
                args.ClassMethodName = match.Groups["Value"].Value;
                args.Cshtml = args.Cshtml.Replace(match.Value, string.Empty);
                if (!args.IsNotClassMethodOverride.HasValue)
                {
                    args.IsNotClassMethodOverride = true;
                }
            }
            else
            {
                args.ClassMethodName = "RenderView";
            }
        }

        #endregion
    }
}