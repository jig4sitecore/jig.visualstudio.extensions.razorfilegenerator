﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// The resolve class name PreProcessor.
    /// </summary>
    internal sealed class ResolveClassNameDirectivePreProcessor : IRazorTemplateCompilerPreProcessor
    {
        #region Fields

        /// <summary>
        ///     The class name regex
        /// </summary>
        private readonly Regex classNameRegex =
            new Regex(
                @"(?:@\x2A \s{1,})(?<Setting>(?<Key>ClassName)(?:\s{1,})=(?:\s{1,})(?<Value>(\w|\.|_)+))(?:\s{1,})(?:\x2A@)\s*\n?\r?",
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Execute(IRazorTemplateCompilerPreProcessorArgs args)
        {
            /* The class name in Cshtml file takes precedent over file name  */
            var match = this.classNameRegex.Match(args.OriginalCshtml);
            if (match.Success)
            {
                args.ClassName = match.Groups["Value"].Value;
                args.Cshtml = args.Cshtml.Replace(match.Value, string.Empty);
            }
        }

        #endregion
    }
}