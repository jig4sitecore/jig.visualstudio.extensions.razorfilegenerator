﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// The resolve is override method PreProcessor.
    /// </summary>
    internal sealed class ResolveIsOverrideMethodDirectivePreProcessor : IRazorTemplateCompilerPreProcessor
    {
        #region Fields

        /// <summary>
        /// The class method is override regex
        /// </summary>
        private readonly Regex classMethodIsOverrideRegex =
            new Regex(
                @"(?:@\x2A \s{1,})(?<Setting>(?<Key>IsClassMethodOverride|IsOverride|Override)(?:\s{1,})=(?:\s{1,})(?<Value>(\w|\.|_)+))(?:\s{1,})(?:\x2A@)\s*\n?\r?",
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Execute(IRazorTemplateCompilerPreProcessorArgs args)
        {
            var match = this.classMethodIsOverrideRegex.Match(args.OriginalCshtml);
            if (match.Success)
            {
                var value = match.Groups["Value"].Value;
                args.Cshtml = args.Cshtml.Replace(match.Value, string.Empty);
 
                bool setting;
                if (bool.TryParse(value, out setting))
                {
                    args.IsNotClassMethodOverride = !setting;
                }
            }
        }

        #endregion
    }
}