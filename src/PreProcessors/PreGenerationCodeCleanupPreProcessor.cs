﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors
{
    using System;
    using System.Linq;

    /// <summary>
    /// The pre generation code cleanup PreProcessor.
    /// </summary>
    internal sealed class PreGenerationCodeCleanupPreProcessor : IRazorTemplateCompilerPreProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// Executes the specified arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        public void Execute(IRazorTemplateCompilerPreProcessorArgs args)
        {
            if (args.RemoveEmptyLinesFromRazorFileInput)
            {
                var lines =
                    args.Cshtml.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries)
                        .Where(x => !string.IsNullOrWhiteSpace(x));

                var code = string.Join(Environment.NewLine, lines);
                args.Cshtml = code;
            }
        }

        #endregion
    }
}