﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// The resolve remove empty lines directive PreProcessor.
    /// </summary>
    internal sealed class ResolveRemoveInputEmptyLinesDirectivePreProcessor : IRazorTemplateCompilerPreProcessor
    {
        #region Fields

        /// <summary>
        /// The remove empty lines regex
        /// </summary>
        private readonly Regex removeEmptyLinesRegex =
            new Regex(
                @"(?:@\x2A \s{1,})(?<Setting>(?<Key>RemoveEmptyLines)(?:\s{1,})=(?:\s{1,})(?<Value>(\w|\.|_)+))(?:\s{1,})(?:\x2A@)\s*\n?\r?", 
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Execute(IRazorTemplateCompilerPreProcessorArgs args)
        {
            var match = this.removeEmptyLinesRegex.Match(args.OriginalCshtml);
            if (match.Success)
            {
                var value = match.Groups["Value"].Value;
                args.Cshtml = args.Cshtml.Replace(match.Value, string.Empty);

                /* The default is true. Check for false flag only */
                bool setting;
                if (bool.TryParse(value, out setting))
                {
                    args.RemoveEmptyLinesFromRazorFileInput = setting;
                }
            }
        }

        #endregion
    }
}