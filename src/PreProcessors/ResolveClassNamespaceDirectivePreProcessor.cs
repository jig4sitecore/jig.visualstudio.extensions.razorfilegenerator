﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PreProcessors
{
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// The resolve class namespace PreProcessor.
    /// </summary>
    internal sealed class ResolveClassNamespaceDirectivePreProcessor : IRazorTemplateCompilerPreProcessor
    {
        #region Fields

        /// <summary>
        ///     The namespace regex
        /// </summary>
        private readonly Regex classNamespaceRegex =
            new Regex(
                @"(?:@\x2A \s{1,})(?<Setting>(?<Key>ClassNamespace)(?:\s{1,})=(?:\s{1,})(?<Value>(\w|\.|_)+))(?:\s{1,})(?:\x2A@)\s*\n?\r?", 
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Executes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <exception cref="HttpException">@* ClassNamespace = {NameSpace} *@   Add to the template a class Namespace</exception>
        public void Execute(IRazorTemplateCompilerPreProcessorArgs args)
        {
            if (string.IsNullOrWhiteSpace(args.ClassNamespace))
            {
                var match = this.classNamespaceRegex.Match(args.OriginalCshtml);
                if (match.Success)
                {
                    args.ClassNamespace = match.Groups["Value"].Value;
                    args.Cshtml = args.Cshtml.Replace(match.Value, string.Empty);
                }
                else
                {
                    throw new HttpException(
                        "@* ClassNamespace = {NameSpace} *@   Add to the template a class Namespace");
                }
            }
        }

        #endregion
    }
}