namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    /// <summary>
    /// The RazorTemplateCompilerPreProcessor interface.
    /// </summary>
    public interface IRazorTemplateCompilerPreProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// Executes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        void Execute(IRazorTemplateCompilerPreProcessorArgs args);

        #endregion
    }
}