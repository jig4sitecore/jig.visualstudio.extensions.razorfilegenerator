namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    /// <summary>
    /// Interface IRazorTemplateCompilerPostProcessor
    /// </summary>
    public interface IRazorTemplateCompilerPostProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// Executes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        void Execute(IRazorTemplateCompilerPostProcessorArgs args);

        #endregion
    }
}