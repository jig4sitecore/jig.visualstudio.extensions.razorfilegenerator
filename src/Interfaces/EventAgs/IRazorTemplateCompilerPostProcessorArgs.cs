﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    /// <summary>
    /// Interface IRazor Template Compiler Post Processor Args
    /// </summary>
    public interface IRazorTemplateCompilerPostProcessorArgs : IRazorTemplateCompilerArgs
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the cs file output.
        /// </summary>
        string CsFileOutput { get; set; }

        #endregion
    }
}