﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    /// <summary>
    /// Interface IRazor Template Compiler PreProcessor Args
    /// </summary>
    public interface IRazorTemplateCompilerPreProcessorArgs : IRazorTemplateCompilerArgs
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the Razor CsHtml file input.
        /// </summary>
        /// <value>The CsHtml file input.</value>
        string Cshtml { get; set; }

        #endregion
    }
}