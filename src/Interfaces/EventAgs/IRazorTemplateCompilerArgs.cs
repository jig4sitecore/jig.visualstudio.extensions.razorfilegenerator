﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator
{
    /// <summary>
    /// Interface IRazorTemplateCompilerArgs
    /// </summary>
    public interface IRazorTemplateCompilerArgs
    {
        /// <summary>
        /// Gets or sets the class method name.
        /// </summary>
        /// <value>The name of the class method.</value>
        string ClassMethodName { get; set; }

        /// <summary>
        /// Gets or sets the class name.
        /// </summary>
        string ClassName { get; set; }

        /// <summary>
        /// Gets or sets the class namespace.
        /// </summary>
        string ClassNamespace { get; set; }

        /// <summary>
        /// Gets or sets the is not class method override.
        /// </summary>
        bool? IsNotClassMethodOverride { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [remove empty lines].
        /// </summary>
        /// <value><c>true</c> if [remove empty lines]; otherwise, <c>false</c>.</value>
        /// <remarks>This VS extension is used for Web and Code/Template generation. This is code generation directive.</remarks>
        bool RemoveEmptyLinesFromRazorFileInput { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [remove code empty lines].
        /// </summary>
        /// <value><c>true</c> if [remove code empty lines]; otherwise, <c>false</c>.</value>
        /// <remarks>This VS extension is used for Web and Code/Template generation. This is code generation directive.</remarks>
        bool RemoveCSharpCodeEmptyLines { get; set; }

        /// <summary>
        /// Gets the original CSHTML.
        /// </summary>
        /// <value>The original CSHTML.</value>
        string OriginalCshtml { get;  }
    }
}
