﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PostProcessors
{
    using System;
    using System.Linq;

    /// <summary>
    /// The remove empty lines processors.
    /// </summary>
    internal sealed class RemoveEmptyLinesProcessors : IRazorTemplateCompilerPostProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Execute(IRazorTemplateCompilerPostProcessorArgs args)
        {
            if (args.RemoveCSharpCodeEmptyLines)
            {
                /* Clean up empty lines and Add back empty lines */
                var lines =
                    args.CsFileOutput.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries)
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .ToList();
                var code = string.Join(Environment.NewLine, lines);
                args.CsFileOutput = code;
            }
        }

        #endregion
    }
}