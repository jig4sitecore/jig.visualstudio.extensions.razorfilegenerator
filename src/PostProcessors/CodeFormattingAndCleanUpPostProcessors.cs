﻿namespace Jig.VisualStudio.Extensions.RazorFileGenerator.PostProcessors
{
    using System.Text;

    /// <summary>
    /// The code formatting and clean up post processors.
    /// </summary>
    internal sealed class CodeFormattingAndCleanUpPostProcessors : IRazorTemplateCompilerPostProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// Executes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public void Execute(IRazorTemplateCompilerPostProcessorArgs args)
        {
            var builder = new StringBuilder(args.CsFileOutput);
            builder.Replace("public class", "public partial class");

            builder.Replace("#line hidden", string.Empty);

            /* Manipulate:
             *      public override void RenderContent()  
             */
            builder.Replace(
                "public override void ", 
                args.IsNotClassMethodOverride.GetValueOrDefault() ? "protected void " : "protected override void ");

            builder.Replace("RenderContent()", args.ClassMethodName + "(HtmlTextWriter writer)");
            builder.Replace("WriteAttribute(", "writer.WriteRazorAttribute(");

            /* remove constructor like:
                public ViewPage01()
                {
                }
             **/
            builder.Replace("        public " + args.ClassName + "() {", string.Empty);
            
            var sourceCode = builder.ToString();
            var index = sourceCode.IndexOf('}');
            if (index > 0)
            {
                var chars = sourceCode.ToCharArray();
                chars[index] = ' ';
                sourceCode = new string(chars);
            }

            args.CsFileOutput = sourceCode;
        }

        #endregion
    }
}